#pragma once
#include <eostalk/protocol/block_header.hpp>
#include <eostalk/protocol/transaction.hpp>

namespace eostalk { namespace protocol {

   struct signed_block : public signed_block_header
   {
      checksum_type calculate_merkle_root()const;
      vector<signed_transaction> transactions;
   };

} } // eostalk::protocol

FC_REFLECT_DERIVED( eostalk::protocol::signed_block, (eostalk::protocol::signed_block_header), (transactions) )
