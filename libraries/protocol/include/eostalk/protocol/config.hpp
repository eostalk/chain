/*
 * Copyright (c) 2016 Steemit, Inc., and contributors.
 */
#pragma once

#define EOSTALK_BLOCKCHAIN_VERSION              ( version(0, 0, 0) )
#define EOSTALK_BLOCKCHAIN_HARDFORK_VERSION     ( hardfork_version( EOSTALK_BLOCKCHAIN_VERSION ) )

#ifdef IS_TEST_NET
#define EOSTALK_INIT_PRIVATE_KEY                (fc::ecc::private_key::regenerate(fc::sha256::hash(std::string("init_key"))))
#define EOSTALK_INIT_PUBLIC_KEY_STR             (std::string( eostalk::protocol::public_key_type(EOSTALK_INIT_PRIVATE_KEY.get_public_key()) ))
#define EOSTALK_CHAIN_ID                        (fc::sha256::hash("testnet"))

#define VESTS_SYMBOL  (uint64_t(6) | (uint64_t('V') << 8) | (uint64_t('E') << 16) | (uint64_t('S') << 24) | (uint64_t('T') << 32) | (uint64_t('S') << 40)) ///< VESTS with 6 digits of precision
#define EOSTALK_SYMBOL  (uint64_t(3) | (uint64_t('T') << 8) | (uint64_t('E') << 16) | (uint64_t('S') << 24) | (uint64_t('T') << 32) | (uint64_t('S') << 40)) ///< TESTS with 3 digits of precision

#define EOSTALK_SYMBOL_STR                      "TEST"
#define EOSTALK_ADDRESS_PREFIX                  "TST"

#define EOSTALK_GENESIS_TIME                    (fc::time_point_sec(1515765200))
#define EOSTALK_CASHOUT_WINDOW_SECONDS          (60*60) /// 1 hr
#define EOSTALK_UPVOTE_LOCKOUT                  (fc::minutes(5))

#define EOSTALK_MIN_ACCOUNT_CREATION_FEE          0

#define EOSTALK_OWNER_AUTH_RECOVERY_PERIOD                  fc::seconds(60)
#define EOSTALK_ACCOUNT_RECOVERY_REQUEST_EXPIRATION_PERIOD  fc::seconds(12)
#define EOSTALK_OWNER_UPDATE_LIMIT                          fc::seconds(0)
#define EOSTALK_OWNER_AUTH_HISTORY_TRACKING_START_BLOCK_NUM 1
#else // IS LIVE NETWORK

#define EOSTALK_INIT_PUBLIC_KEY_STR             "ETK5CtdyWGQ2terf2reYceADNf2JcuePQc6j59WhLRbTQs4GpP3ik"
#define EOSTALK_CHAIN_ID                        (fc::sha256::hash("eostalk")) // e1263b22c44f1ed101c7f50f4ee44ed66427d9ffcb20a0cf14d66fc2fbdb9e85
#define VESTS_SYMBOL  (uint64_t(6) | (uint64_t('V') << 8) | (uint64_t('E') << 16) | (uint64_t('S') << 24) | (uint64_t('T') << 32) | (uint64_t('S') << 40)) ///< VESTS with 6 digits of precision
#define EOSTALK_SYMBOL  (uint64_t(3) | (uint64_t('E') << 8) | (uint64_t('O') << 16) | (uint64_t('S') << 24) | (uint64_t('B') << 32) | (uint64_t('I') << 40) | (uint64_t('T') << 48)) ///< EOSBIT with 3 digits of precision
#define EOSTALK_SYMBOL_STR                      "EOSBIT"
#define EOSTALK_ADDRESS_PREFIX                  "ETK"

#define EOSTALK_GENESIS_TIME_EPOCH              1529900000  // Mon Jun 25 2018 11:13:20 GMT+0700
#define EOSTALK_GENESIS_TIME                    (fc::time_point_sec(EOSTALK_GENESIS_TIME_EPOCH))
#define EOSTALK_CASHOUT_WINDOW_SECONDS          (60*60*24*7)  /// 7 days
#define EOSTALK_UPVOTE_LOCKOUT                  (fc::hours(12))

#define EOSTALK_MIN_ACCOUNT_CREATION_FEE         10 // 1

#define EOSTALK_OWNER_AUTH_RECOVERY_PERIOD                  fc::days(30)
#define EOSTALK_ACCOUNT_RECOVERY_REQUEST_EXPIRATION_PERIOD  fc::days(1)
#define EOSTALK_OWNER_UPDATE_LIMIT                          fc::minutes(60)
#define EOSTALK_OWNER_AUTH_HISTORY_TRACKING_START_BLOCK_NUM 1 // 3186477

#endif

#define EOSTALK_BLOCK_INTERVAL                  3
#define EOSTALK_BLOCKS_PER_YEAR                 (365*24*60*60/EOSTALK_BLOCK_INTERVAL)
#define EOSTALK_BLOCKS_PER_DAY                  (24*60*60/EOSTALK_BLOCK_INTERVAL)
#define EOSTALK_START_MINER_VOTING_BLOCK        (EOSTALK_BLOCKS_PER_DAY * 30)

#define EOSTALK_MAX_WITNESSES                   21

#define EOSTALK_INIT_MINER_NAME                 "initminer"
#define EOSTALK_NUM_INIT_MINERS                 (EOSTALK_MAX_WITNESSES-1)

#define EOSTALK_MAX_VOTED_WITNESSES             20
#define EOSTALK_MAX_MINER_WITNESSES             0
#define EOSTALK_MAX_RUNNER_WITNESSES            1

#define EOSTALK_HARDFORK_REQUIRED_WITNESSES     17 // 17 of the 21 dpos witnesses (20 elected and 1 virtual time) required for hardfork. This guarantees 75% participation on all subsequent rounds.
#define EOSTALK_MAX_TIME_UNTIL_EXPIRATION       (60*60) // seconds,  aka: 1 hour
#define EOSTALK_MAX_MEMO_SIZE                   2048
#define EOSTALK_MAX_PROXY_RECURSION_DEPTH       4
#define EOSTALK_VESTING_WITHDRAW_INTERVALS      13
#define EOSTALK_VESTING_WITHDRAW_INTERVAL_SECONDS (60*60*24*7) /// 1 week per interval

#define EOSTALK_MAX_WITHDRAW_ROUTES             10
#define EOSTALK_VOTE_REGENERATION_SECONDS       (5*60*60*24) // 5 day
#define EOSTALK_MAX_VOTE_CHANGES                5
#define EOSTALK_REVERSE_AUCTION_WINDOW_SECONDS  (60*30) /// 30 minutes
#define EOSTALK_MIN_VOTE_INTERVAL_SEC           3
#define EOSTALK_VOTE_DUST_THRESHOLD             (50000000)

#define EOSTALK_MIN_ROOT_COMMENT_INTERVAL       (fc::seconds(60*5)) // 5 minutes
#define EOSTALK_MIN_REPLY_INTERVAL              (fc::seconds(20)) // 20 seconds

#define EOSTALK_MAX_ACCOUNT_WITNESS_VOTES       30

#define EOSTALK_100_PERCENT                     10000
#define EOSTALK_1_PERCENT                       (EOSTALK_100_PERCENT/100)
#define EOSTALK_1_TENTH_PERCENT                 (EOSTALK_100_PERCENT/1000)

#define EOSTALK_INFLATION_RATE_START_PERCENT    (978) // 9.5%
#define EOSTALK_INFLATION_RATE_STOP_PERCENT     (487) // 4.87%
#define EOSTALK_INFLATION_NARROWING_PERIOD      (250000) // Narrow 0.01% every 250k blocks
#define EOSTALK_CONTENT_REWARD_PERCENT          (75*EOSTALK_1_PERCENT) //75% of inflation, 7.125% inflation
#define EOSTALK_VESTING_FUND_PERCENT            (15*EOSTALK_1_PERCENT) //15% of inflation, 1.425% inflation

#define EOSTALK_CONTENT_CURATE_REWARD_PERCENT   (50*EOSTALK_1_PERCENT) // % of EOSTALK_CONTENT_REWARD_PERCENT only

#define EOSTALK_BANDWIDTH_AVERAGE_WINDOW_SECONDS (60*60*24*7) ///< 1 week
#define EOSTALK_BANDWIDTH_PRECISION             (uint64_t(1000000)) ///< 1 million
#define EOSTALK_MAX_COMMENT_DEPTH               0xffff // 64k
#define EOSTALK_SOFT_MAX_COMMENT_DEPTH          0xff // 255

#define EOSTALK_MAX_RESERVE_RATIO               (20000)

#define EOSTALK_CREATE_ACCOUNT_DELEGATION_RATIO    5
#define EOSTALK_CREATE_ACCOUNT_DELEGATION_TIME     fc::days(30)

#define EOSTALK_ACTIVE_CHALLENGE_FEE            asset( 2000, EOSTALK_SYMBOL )
#define EOSTALK_OWNER_CHALLENGE_FEE             asset( 30000, EOSTALK_SYMBOL )

#define EOSTALK_POST_REWARD_FUND_NAME           ("post")
#define EOSTALK_RECENT_RSHARES_DECAY_RATE       (fc::days(15))
#define EOSTALK_CONTENT_CONSTANT                (uint128_t(uint64_t(2000000000000ll)))

#define EOSTALK_MIN_PAYOUT_STEEM                20
#define EOSTALK_MIN_ACCOUNT_NAME_LENGTH          3
#define EOSTALK_MAX_ACCOUNT_NAME_LENGTH         16

#define EOSTALK_MIN_PERMLINK_LENGTH             0
#define EOSTALK_MAX_PERMLINK_LENGTH             256
#define EOSTALK_MAX_WITNESS_URL_LENGTH          2048

#define EOSTALK_INIT_SUPPLY                     int64_t(12000000000ll) // 12m EOSBIT
#define EOSTALK_MAX_SHARE_SUPPLY                int64_t(1000000000000000ll)
#define EOSTALK_MAX_SIG_CHECK_DEPTH             2

#define EOSTALK_SECONDS_PER_YEAR                (uint64_t(60*60*24*365ll))

#define EOSTALK_MAX_TRANSACTION_SIZE            (1024*64)
#define EOSTALK_MIN_BLOCK_SIZE_LIMIT            (EOSTALK_MAX_TRANSACTION_SIZE)
#define EOSTALK_MAX_BLOCK_SIZE                  (EOSTALK_MAX_TRANSACTION_SIZE*EOSTALK_BLOCK_INTERVAL*2000)
#define EOSTALK_SOFT_MAX_BLOCK_SIZE             (2*1024*1024)
#define EOSTALK_MIN_BLOCK_SIZE                  115
#define EOSTALK_BLOCKS_PER_HOUR                 (60*60/EOSTALK_BLOCK_INTERVAL)
#define EOSTALK_MIN_UNDO_HISTORY                10
#define EOSTALK_MAX_UNDO_HISTORY                10000

#define EOSTALK_MIN_TRANSACTION_EXPIRATION_LIMIT (EOSTALK_BLOCK_INTERVAL * 5) // 5 transactions per block

#define EOSTALK_IRREVERSIBLE_THRESHOLD          (75 * EOSTALK_1_PERCENT)
#define VIRTUAL_SCHEDULE_LAP_LENGTH           ( fc::uint128::max_value() )

/**
 *  Reserved Account IDs with special meaning
 */
///@{
/// Represents the current witnesses
#define EOSTALK_MINER_ACCOUNT                   "miners"
/// Represents the canonical account with NO authority (nobody can access funds in null account)
#define EOSTALK_NULL_ACCOUNT                    "null"
/// Represents the canonical account with WILDCARD authority (anybody can access funds in temp account)
#define EOSTALK_TEMP_ACCOUNT                    "temp"
/// Represents the canonical account for specifying you will vote for directly (as opposed to a proxy)
#define EOSTALK_PROXY_TO_SELF_ACCOUNT           ""
/// Represents the canonical root post parent account
#define EOSTALK_ROOT_POST_PARENT                (account_name_type())
///@}
