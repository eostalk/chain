#pragma once

#include <fc/variant_object.hpp>

namespace eostalk { namespace protocol {

fc::variant_object get_config();

} } // eostalk::protocol
