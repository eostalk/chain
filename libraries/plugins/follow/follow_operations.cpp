#include <eostalk/follow/follow_operations.hpp>

#include <eostalk/protocol/operation_util_impl.hpp>

namespace eostalk { namespace follow {

void follow_operation::validate()const
{
   FC_ASSERT( follower != following, "You cannot follow yourself" );
}

void reblog_operation::validate()const
{
   FC_ASSERT( account != author, "You cannot reblog your own content" );
}

} } //eostalk::follow

DEFINE_OPERATION_TYPE( eostalk::follow::follow_plugin_operation )
