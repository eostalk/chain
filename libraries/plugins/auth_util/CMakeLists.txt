file(GLOB HEADERS "include/eostalk/plugins/auth_util/*.hpp")

add_library( eostalk_auth_util
             ${HEADERS}
             auth_util_plugin.cpp
             auth_util_api.cpp
           )

target_link_libraries( eostalk_auth_util eostalk_app eostalk_chain eostalk_protocol fc )
target_include_directories( eostalk_auth_util
                            PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include" )
