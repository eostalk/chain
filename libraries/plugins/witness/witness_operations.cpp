#include <eostalk/witness/witness_operations.hpp>

#include <eostalk/protocol/operation_util_impl.hpp>

namespace eostalk { namespace witness {

void enable_content_editing_operation::validate()const
{
   chain::validate_account_name( account );
}

} } // eostalk::witness

DEFINE_OPERATION_TYPE( eostalk::witness::witness_plugin_operation )
