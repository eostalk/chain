file(GLOB HEADERS "include/eostalk/plugins/block_info/*.hpp")

add_library( eostalk_block_info
             ${HEADERS}
             block_info_plugin.cpp
             block_info_api.cpp
           )

target_link_libraries( eostalk_block_info eostalk_app eostalk_chain eostalk_protocol fc )
target_include_directories( eostalk_block_info
                            PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include" )
