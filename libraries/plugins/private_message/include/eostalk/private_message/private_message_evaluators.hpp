#pragma once

#include <eostalk/chain/evaluator.hpp>

#include <eostalk/private_message/private_message_operations.hpp>
#include <eostalk/private_message/private_message_plugin.hpp>

namespace eostalk { namespace private_message {

DEFINE_PLUGIN_EVALUATOR( private_message_plugin, eostalk::private_message::private_message_plugin_operation, private_message )

} }
