file(GLOB HEADERS "include/eostalk/private_message/*.hpp")

add_library( eostalk_private_message
             private_message_plugin.cpp
           )

target_link_libraries( eostalk_private_message eostalk_chain eostalk_protocol eostalk_app )
target_include_directories( eostalk_private_message
                            PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include" )

install( TARGETS
    eostalk_private_message

   RUNTIME DESTINATION bin
   LIBRARY DESTINATION lib
   ARCHIVE DESTINATION lib
)
