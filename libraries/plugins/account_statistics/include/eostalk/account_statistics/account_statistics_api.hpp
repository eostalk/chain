#pragma once

#include <eostalk/account_statistics/account_statistics_plugin.hpp>

#include <fc/api.hpp>

namespace eostalk{ namespace app {
   struct api_context;
} }

namespace eostalk { namespace account_statistics {

namespace detail
{
   class account_statistics_api_impl;
}

class account_statistics_api
{
   public:
      account_statistics_api( const eostalk::app::api_context& ctx );

      void on_api_startup();

   private:
      std::shared_ptr< detail::account_statistics_api_impl > _my;
};

} } // eostalk::account_statistics

FC_API( eostalk::account_statistics::account_statistics_api, )