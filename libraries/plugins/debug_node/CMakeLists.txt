file(GLOB HEADERS "include/eostalk/plugins/debug_node/*.hpp")

add_library( eostalk_debug_node
             ${HEADERS}
             debug_node_plugin.cpp
             debug_node_api.cpp
           )

target_link_libraries( eostalk_debug_node eostalk_app eostalk_chain eostalk_protocol fc )
target_include_directories( eostalk_debug_node
                            PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include" )

install( TARGETS eostalk_debug_node
         RUNTIME DESTINATION bin
         LIBRARY DESTINATION lib
         ARCHIVE DESTINATION lib
       )
INSTALL( FILES ${HEADERS} DESTINATION "include/eostalk/plugins/debug_node" )
