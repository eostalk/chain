
#pragma once

#include <memory>
#include <string>
#include <vector>

namespace eostalk { namespace app {

class abstract_plugin;
class application;

} }

namespace eostalk { namespace plugin {

void initialize_plugin_factories();
std::shared_ptr< eostalk::app::abstract_plugin > create_plugin( const std::string& name, eostalk::app::application* app );
std::vector< std::string > get_available_plugins();

} }
