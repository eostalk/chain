# Introducing eostalk (beta)

eostalk is a Steem fork for EOS community.

  - Currency symbol EOSBIT.
  - Initial supply 42,000,000 EOSBIT
  - inflation rate of 9.5% per year, reducing by 0.5% per annum until the rewards
    drop to 5% per annum.
  - 75% of inflation to social consensus algorithm.
  - 15% of inflation to stake holders.
  - 10% of inflation to block producers.
