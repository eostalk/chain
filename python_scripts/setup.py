from setuptools import setup

setup( name='eostalkdebugnode',
       version='0.1',
       description='A wrapper for launching and interacting with a Smoke Debug Node',
       url='https://github.com/eostalk/eostalkd',
       author='Smoke, Inc.',
       author_email='hello@eostalk.io',
       license='See LICENSE.md',
       packages=['eostalkdebugnode'],
       #install_requires=['eostalkapi'],
       zip_safe=False )