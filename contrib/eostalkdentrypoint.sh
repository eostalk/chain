#!/bin/bash

echo /tmp/core | tee /proc/sys/kernel/core_pattern
ulimit -c unlimited

# if we're not using PaaS mode then start eostalkd traditionally with sv to control it
if [[ ! "$USE_PAAS" ]]; then
  mkdir -p /etc/service/eostalkd
  cp /usr/local/bin/eostalk-sv-run.sh /etc/service/eostalkd/run
  chmod +x /etc/service/eostalkd/run
  runsv /etc/service/eostalkd
else
  /usr/local/bin/startpaaseostalkd.sh
fi
